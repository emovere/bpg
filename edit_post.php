<?php
    session_start();
    ob_start();
    include_once("db.php");
    
    if(!isset($_SESSION['id'])){
        header("Location: login.php");
    }
    $url = $_GET['page'];

    if($url == "aktualnosci"){
         if(!isset($_GET['pid'])){
            header("Location: panel.php");

        }

        $pid = $_GET['pid'];

        if(isset($_POST['edytuj'])){
            $tytul = strip_tags($_POST['tytul']);
            $zawartosc = strip_tags($_POST['zawartosc']);

            $tytul = mysqli_real_escape_string($db, $tytul);
            $zawartosc =mysqli_real_escape_string($db, $zawartosc);

            $data = date('Y-m-d');

            $sql = "UPDATE aktualnosci SET tytul= '$tytul',zawartosc='$zawartosc',data='$data' WHERE id=$pid";

            if($tytul == ""  || $zawartosc == "") {
                echo "Proszę wypełnić formularz!";
                return;
            }

            mysqli_query($db,$sql);

            header("location: panel.php?page=aktualnosci");
        }
        echo "<!doctype html>";
        echo "<html>";
        echo "<head>";
        echo "<title>Aktualnosci-Post</title>";
        echo "</head>";
        echo "<body> ";
        $sql_get = "SELECT * FROM aktualnosci WHERE id=$pid LIMIT 1";
        $res = mysqli_query($db,$sql_get);
        if(mysqli_num_rows($res) > 0){
            while ($row = mysqli_fetch_assoc($res)){
                $tytul = $row['tytul'];
                $zawartosc = $row['zawartosc'];
                echo "<form action='edit_post.php?pid=$pid&page=aktualnosci' method='post' enctype='multipart/form-data'>";
                echo "<input placeholder='Tytuł' name='tytul' type='text' value='$tytul' autofocus size='48'><br /><br />";
                echo "<textarea  placeholder='Zawartość' name='zawartosc' id='' cols='75' rows='12'>$zawartosc</textarea><br /><br/><input name='edytuj' type='submit' value='Edytuj'></form>";
            }
        }
        echo "</body>";
        echo "</html>";
    }
    if($url == "ofirmie"){
         if(!isset($_GET['pid'])){
            header("Location: panel.php");

        }

        $pid = $_GET['pid'];

        if(isset($_POST['edytuj'])){
            $zawartosc = strip_tags($_POST['zawartosc']);
            $zawartosc =mysqli_real_escape_string($db, $zawartosc);
            $sql = "UPDATE ofirmie SET zawartosc='$zawartosc' WHERE id=$pid";

            if($zawartosc == "") {
                echo "Proszę wypełnić formularz!";
                return;
            }

            mysqli_query($db,$sql);

            header("location: panel.php?page=ofirmie");
        }
        echo "<!doctype html>";
        echo "<html>";
        echo "<head>";
        echo "<title>Aktualnosci-Post</title>";
        echo "</head>";
        echo "<body> ";
        $sql_get = "SELECT * FROM ofirmie WHERE id=$pid LIMIT 1";
        $res = mysqli_query($db,$sql_get);
        if(mysqli_num_rows($res) > 0){
            while ($row = mysqli_fetch_assoc($res)){
                $zawartosc = $row['zawartosc'];
                echo "<form action='edit_post.php?pid=$pid&page=ofirmie' method='post' enctype='multipart/form-data'>";
                echo "<textarea  placeholder='Zawartość' name='zawartosc' id='' cols='50' rows='20'>$zawartosc</textarea><br /><br/><input name='edytuj' type='submit' value='Edytuj'></form>";
            }
        }
        echo "</body>";
        echo "</html>";
    }

    if($url == "oferta"){
         if(!isset($_GET['pid'])){
            header("Location: panel.php");

        }

        $pid = $_GET['pid'];

        if(isset($_POST['edytuj'])){
            $zawartosc = strip_tags($_POST['zawartosc']);
            $zawartosc =mysqli_real_escape_string($db, $zawartosc);
            $sql = "UPDATE oferta SET zawartosc='$zawartosc' WHERE id=$pid";

            if($zawartosc == "") {
                echo "Proszę wypełnić formularz!";
                return;
            }

            mysqli_query($db,$sql);

            header("location: panel.php?page=oferta");
        }
        echo "<!doctype html>";
        echo "<html>";
        echo "<head>";
        echo "<title>Edit</title>";
        echo "</head>";
        echo "<body> ";
        $sql_get = "SELECT * FROM oferta WHERE id=$pid LIMIT 1";
        $res = mysqli_query($db,$sql_get);
        if(mysqli_num_rows($res) > 0){
            while ($row = mysqli_fetch_assoc($res)){
                $zawartosc = $row['zawartosc'];
                echo "<form action='edit_post.php?pid=$pid&page=oferta' method='post' enctype='multipart/form-data'>";
                echo "<textarea  placeholder='Zawartość' name='zawartosc' id='' cols='50' rows='20'>$zawartosc</textarea><br /><br/><input name='edytuj' type='submit' value='Edytuj'></form>";
            }
        }
        echo "</body>";
        echo "</html>";
    }
    ob_end_flush();
?>