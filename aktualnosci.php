<section id = "aktualnosci">
  <div id = "aktualnosci-block">
   <?php 
    include_once("db.php");
    require_once("nbbc/nbbc.php");
    
    $bbcode= new BBCode;
    
    $sql = "SELECT * FROM aktualnosci order by id DESC";

    $res = mysqli_query($db, $sql) or die(mysqli_error());

    $posts = "";

    if(mysqli_num_rows($res) > 0){
        while($row = mysqli_fetch_assoc($res)){
            $id = $row['id'];
            $tytul = $row['tytul'];
            $zawartosc = $row['zawartosc'];
            $data = $row['data'];
            $obraz = $row['obraz'];

            $zawartosc = $bbcode->Parse($zawartosc);
            
            $posts .= "<div id='aktualnosci-tab'><article id = 'aktualnosci-wrapper-picture'>
            <img src = '$obraz' alt = ''>
            </article>
            <aside>
            <div id = 'aktualnosci-wrapper-art'><h2>$tytul</h2>
            <h3>$data</h3>
            <p>$zawartosc</p></div></div>";
        }
        echo $posts;
    } else{
        echo "<h2>Brak aktualności</h2>";
    }
?>
    </div>
</section>