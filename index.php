﻿<?php
(empty($_GET['page'])) ? $url = "start" : $url = $_GET['page'];
include_once("db.php");
?>

<!DOCTYPE html>

<html>
<head>
    <title>BPG</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/styles.css" type="text/css" rel="stylesheet"/>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
</head>
<body background="Tło.png"></body>
<body>
<div id="main">
    <section id="slider-section">
        <div id="slider-tag">
            <a href=index.php?page=start></a>
        </div>
    </section>

    <!--SLIDER ENDS HERE-->

    <header>
        <div id="header-inner">
            <nav>
                <label for="toggle"><img src="images/hamburger.png" width="40" height="40" alt=""></label>
                <input type="checkbox" id="toggle"/>
                <ul id="navigation-toolbar">
                    <li class="menu"><img src="images/separator.png"><a href="index.php?page=start" class="current">Start</a>
                    </li>
                    <li class="menu"><img src="images/separator.png"><a href="index.php?page=aktualnosci"
                                                                        class="current">Aktualności</a></li>
                    <li class="menu"><img src="images/separator.png"><a href="index.php?page=ofirmie" class="current">O
                            firmie</a></li>
                    <li class="menu"><img src="images/separator.png"><a href="index.php?page=oferta" class="current">Oferta</a>
                    </li>
                    <li class="menu"><img src="images/separator.png"><a href="index.php?page=projekty" class="current">Rodo</a>
                    </li>
                    <li class="menu-end"><img src="images/separator.png"><a href="index.php?page=kontakt"
                                                                            class="current">Kontakt</a>
                        <img src="images/separator.png"></li>
                </ul>
            </nav>
        </div>
    </header>

    <?php
    (empty($_GET['page'])) ? $url = "start" : $url = $_GET['page'];
    include_once("db.php");
    include $url . ".php";
    ?>
    <footer>
        <section id="footer-inner">
            <section class="footer-wrapper">
                <article class="footer-lefttext">
                    <p>
                        <nobr>Biuro Projektowe</nobr>
                    </p>
                    <p>
                        <nobr>Gór-Spec Sp. z o.o</nobr>
                    </p>
                    <p>44-240 Żory,</p>
                    <p>ul. Modrzewiowa 15</p>
                </article>
                <article class="footer-middletext">
                    <p>Telefon:
                        <nobr>+48 600 377 801</nobr>
                    </p>
                    <p>E-mail:
                        <br>
                        <a href="mailto: biuro@bpg-s.pl" style="color: white;">biuro@bpg-s.pl</a>
                        <br>
                        <a href="mailto: sekretariat@bpg-s.pl" style="color: white;">sekretariat@bpg-s.pl</a>
                    </p>
                </article>
                <ul class="footer-last">
                    <li class=foot_menu><a href="index.php?page=start">Start ></a></li>
                    <li class=foot_menu><a href="index.php?page=aktualnosci">Aktualności ></a></li>
                    <li class=foot_menu><a href="index.php?page=ofirmie">O firmie ></a></li>
                    <li class=foot_menu><a href="index.php?page=oferta">Oferta ></a></li>
                    <li class=foot_menu><a href="index.php?page=projekty">Rodo ></a></li>
                    <li class=foot_menu><a href="index.php?page=kontakt">Kontakt ></a></li>
                </ul>
            </section>
        </section>
        <div id="stopka">
            <p>
                Biuro Projektowe Gór-Spec sp. z o.o. ©2017. All rights reserved.
                <br>Projekt i Wykonanie: Amadeusz Zięba
                <br>Zdjęcia: Vasylina Kityk
                <br>Kontakt: <a href="mailto: projekty.ama@gmail.com">projekty.ama@gmail.com</a>
            </p>
        </div>
    </footer>
</div>
</body>
</html>