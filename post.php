﻿<?php
    session_start();
    ob_start();
    if(!isset($_SESSION['id'])){
        header("Location: login.php");
    }
    include_once("db.php");
    (empty($_GET['page'])) ? $url = "" : $url = $_GET['page'];

    if($url == "aktualnosci"){
        if(isset($_POST['post'])){
            $tytul = strip_tags($_POST['tytul']);
            $zawartosc = strip_tags($_POST['zawartosc']);
            $target = "images/aktualnosci/".basename($_FILES['obraz']['name']);

            $tytul = mysqli_real_escape_string($db, $tytul);
            $zawartosc =mysqli_real_escape_string($db, $zawartosc);
            $obraz = mysqli_real_escape_string($db, $target);

            $data = date('Y-m-d');

            $sql = "INSERT INTO aktualnosci (tytul, zawartosc, data, obraz) VALUES ('$tytul','$zawartosc','$data','$obraz')";

            if($tytul == ""  || $zawartosc == "") {
                echo "Proszę wypełnić formularz!";
                return;
            }

            if(move_uploaded_file($_FILES["obraz"]["tmp_name"], $target)) {
                echo "Obraz załadowany poprawnie!";
            }
            else{
                echo "Wystąpił problem z załadowaniem obrazu.";
            }

            mysqli_query($db,$sql);

            header("location: panel.php?page=aktualnosci");
        }
        
        echo "<!doctype html>
        <html>
        <head>
            <title>Aktualnosci-Post</title>
        </head>
        <body>
            <form action='post.php?page=aktualnosci' method='post' enctype='multipart/form-data'>
                <input placeholder='Tytuł' name='tytul' type='text' autofocus size='48'><br /><br />
                <textarea  placeholder='Zawartość' name='zawartosc' id='' cols='75' rows='12'></textarea><br /><br />
                <input type='file' name='obraz'>
                <input name='post' type='submit' value='Post'>
            </form>
        </body>
        </html>";
    }
    if($url == "ofirmie"){
        if(isset($_POST['post'])){
                $zawartosc = strip_tags($_POST['zawartosc']);
                $target = "images/".basename($_FILES['obraz']['name']);

                $zawartosc =mysqli_real_escape_string($db, $zawartosc);
                $obraz = mysqli_real_escape_string($db, $target);

                $data = date('Y-m-d');

                $sql = "INSERT INTO ofirmie (zawartosc, obraz) VALUES ('$zawartosc','$obraz')";

                if($zawartosc == "") {
                    echo "Proszę wypełnić formularz!";
                    return;
                }

                if(move_uploaded_file($_FILES["obraz"]["tmp_name"], $target)) {
                    echo "Obraz załadowany poprawnie!";
                }
                else{
                    echo "Wystąpił problem z załadowaniem obrazu.";
                }

                mysqli_query($db,$sql);

                header("location: panel.php?page=ofirmie");
            }

            echo "<!doctype html>
            <html>
            <head>
                <title>Aktualnosci-Post</title>
            </head>
            <body>
                <form action='post.php?page=ofirmie' method='post' enctype='multipart/form-data'>
                    <textarea  placeholder='Zawartość' name='zawartosc' id='' cols='100' rows='50'></textarea><br /><br />
                    <input type='file' name='obraz'>
                    <input name='post' type='submit' value='Post'>
                </form>
            </body>
            </html>";
    }
    if($url == "oferta"){
        if(isset($_POST['post'])){
            $zawartosc = strip_tags($_POST['zawartosc']);
            $zawartosc =mysqli_real_escape_string($db, $zawartosc);

            $sql = "INSERT INTO oferta (zawartosc) VALUES ('$zawartosc')";

            if($zawartosc == "") {
                echo "Proszę wypełnić formularz!";
                return;
            }

            mysqli_query($db,$sql);

            header("location: panel.php?page=oferta");
        }
        
        echo "<!doctype html>
        <html>
        <head>
            <title>Aktualnosci-Post</title>
        </head>
        <body>
            <form action='post.php?page=oferta' method='post' enctype='multipart/form-data'>
                <textarea  placeholder='Zawartość' name='zawartosc' id='' cols='100' rows='50'></textarea><br /><br />
                <input name='post' type='submit' value='Post'>
            </form>
        </body>
        </html>";
    }
    ob_end_flush();
?>