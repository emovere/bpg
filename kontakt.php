﻿<div id="kontakt">
    <div id="kontakt-info">
        <h1 class="title">KONTAKT</h1>       
        <h2>Biuro Projektowe <br> Gór-Spec Sp. z o.o.</h2>
        <p>
            ul. Modrzewiowa 15<br>
            44-240 Żory<br>
            NIP: 6511737963<br>
            REGON: 387931916<br>
            KRS: 00000878468 Sąd Rejonowy w Gliwicach,
            X Wydział Gospodarczy Krajowego Rejestru Sądowego<br>
            tel: +48 600 377 801<br>
            E-mail: <nobr><a href="mailto: biuro@bpg-s.pl">biuro@bpg-s.pl</a></nobr>
        </p></div>
    <div id="map"></div>
</div>
<script>
    function initMap(){
        var location = {lat: 50.050566, lng: 18.709808};
        var map = new google.maps.Map(document.getElementById("map"),{
            zoom: 16,
            center: location
        });
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQLS2l1KAdEq3w_BUJ80Ts2BNCWkA89kg&callback=initMap"
  type="text/javascript">
</script>