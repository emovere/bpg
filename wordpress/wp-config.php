<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'qxK*Wr`0bQ:s$.ixryq/nKM,0]AaG5-Z,LV>zEzDztz[o]>ISHqQbrP,`<*U:.rw');
define('SECURE_AUTH_KEY',  'AUL3}TwdS5&3=c#2L d{d#FD()PxTq@G17@EMz/h*b>*te>4c/lQ*mrQ?a@lLO?N');
define('LOGGED_IN_KEY',    '?-QA]PWr_etg3-E_o/t:]HSAzR>gpq^n78Adu9jQ-ioxDbVqC/%.[aW-&t=@IZw_');
define('NONCE_KEY',        'W3$RZ+~h&LhZt5nH+j=8m+3TKi]@|Y8cJhGKkF=5$:^B8#?OX*#+l{@VENmG)`t@');
define('AUTH_SALT',        'qS>^(eYFQO1 S;Q6Z/Fwmi1.@_X^q#pe&Bx+Z>4~o+]MX/^&[Rzg$8RpvdhfASsg');
define('SECURE_AUTH_SALT', 'EoVwS}qKx^I(I0|hI`{U%--_EOnHCfkO9zIf(8yTg.^us$R0z-b,do(g!J1HGe<;');
define('LOGGED_IN_SALT',   '{lB:ZJC6-IF3j;Bjiw)i>?T0}J-Up~J#(F+>i9d).9]Lfui7i$Z2HvwXf)k}&Ud[');
define('NONCE_SALT',       '@F%;sTUxTU4Zqm~n3RMws!*g>W+#S]QdL;;&~^DV;S/h^D8FqT(wZAxndSM%h:3C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
