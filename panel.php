<?php 
    session_start();

    if(!isset($_SESSION['id'])){
        header("Location: login.php");
    }
    include_once("db.php"); require_once("nbbc/nbbc.php");
    echo "<a href='panel.php?page=aktualnosci'><input type='submit' value='aktualnosci'></a>";
    echo "<a href='panel.php?page=ofirmie'><input type='submit' value='O firmie'></a>";
    echo "<a href='panel.php?page=oferta'><input type='submit' value='oferta'></a>";
    echo "<a href='logout.php'><input type='submit' value='wyloguj'></a>";
    (empty($_GET['page'])) ? $url = "" : $url = $_GET['page'];
    if($url == "aktualnosci"){
        echo "<a href='post.php?page=aktualnosci'><input type='submit' value='nowy'></a>";
        $bbcode= new BBCode;

        $sql = "SELECT * FROM aktualnosci order by id DESC";

        $res = mysqli_query($db, $sql) or die(mysqli_error());

        $posts = "";

        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_assoc($res)){
                $id = $row['id'];
                $tytul = $row['tytul'];
                $zawartosc = $row['zawartosc'];
                $data = $row['data'];
                $obraz = $row['obraz'];

                $admin = "<div><a href='del_post.php?pid=$id&page=aktualnosci'>Delete</a>&nbsp;<a href='edit_post.php?pid=$id&page=aktualnosci'>Edit</a>&nbsp;";

                $zawartosc = $bbcode->Parse($zawartosc);

                $posts .= "<div><h2>$tytul</h2>
                <h3>$data</h3>
                <p>$zawartosc</p>
                <img src='$obraz' alt=''>
                $admin</div><hr />";
            }
            echo $posts;
        } else{
            echo "<h2>Brak aktualności</h2>";
        }
    }
    if($url == "oferta"){
        echo "<a href='post.php?page=oferta'><input type='submit' value='nowa oferta'></a>";
        $bbcode= new BBCode;

        $sql = "SELECT * FROM oferta order by id DESC";

        $res = mysqli_query($db, $sql) or die(mysqli_error());

        $posts = "";

        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_assoc($res)){
                $id = $row['id'];
                $zawartosc = $row['zawartosc'];

                $admin = "<div><a href='del_post.php?pid=$id&page=oferta'>Delete</a>&nbsp;<a href='edit_post.php?pid=$id&page=oferta'>Edit</a>&nbsp;";

                $zawartosc = $bbcode->Parse($zawartosc);

                $posts .= "
                <p>$zawartosc</p>
                $admin</div><hr />";
            }
            echo $posts;
        } else{
            echo "<h2>Brak oferty</h2>";
        }
    }
    if($url == "ofirmie"){
        echo "<a href='post.php?page=ofirmie'><input type='submit' value='nowy'></a>";
        $bbcode= new BBCode;

        $sql = "SELECT * FROM ofirmie order by id DESC";

        $res = mysqli_query($db, $sql) or die(mysqli_error());

        $posts = "";

        if(mysqli_num_rows($res) > 0){
            while($row = mysqli_fetch_assoc($res)){
                $id = $row['id'];
                $zawartosc = $row['zawartosc'];
                $obraz = $row['obraz'];

                $admin = "<div><a href='del_post.php?pid=$id&page=ofirmie'>Delete</a>&nbsp;<a href='edit_post.php?pid=$id&page=ofirmie'>Edit</a>&nbsp;";

                $zawartosc = $bbcode->Parse($zawartosc);

                $posts .= "
                <img src='$obraz' alt=''>
                <p>$zawartosc</p>
                $admin</div><hr />";
            }
            echo $posts;
        } else{
            echo "<h2>Brak informacji o firmie</h2>";
        }
    }
?>
